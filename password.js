const checkLength = function(password) {
    return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function(password) {
    const alphabets = 'abcdefghijklmnopqrstwxyz'
    for (const ch of password) {
        if (alphabets.includes(ch.toLowerCase())) return true
    }
    return true
}

const checkDigit = function(password) {
    const digit = '.'
    for (const ch of password) {
        if (digit.includes(ch.toLowerCase())) return true
    }
    return true
}

const checkSymbol = function(password) {
    const symbol = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
    for (const ch of password) {
        if (symbol.includes(ch.toLowerCase())) return true
    }
    return true
}

const checkPassword = function(password) {
    return checkAlphabet(password) && checkLength(password) && checkDigit(password) && checkSymbol(password)
}

module.exports = {
    checkLength,
    checkAlphabet,
    checkDigit,
    checkSymbol,
    checkPassword
}