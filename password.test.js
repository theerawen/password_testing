const { checkLength, checkAlphabet, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
    test('should 8 characters to be true', () => {
        expect(checkLength('12345678')).toBe(true)
    })
    test('should 7 characters to be flase', () => {
        expect(checkLength('1234567')).toBe(false)
    })
    test('should 25 characters to be flase', () => {
        expect(checkLength('1111122222333334444455555')).toBe(true)
    })
    test('should 26 characters to be flase', () => {
        expect(checkLength('11111222223333344444555556')).toBe(false)
    })
})
describe('Test Aplabet', () => {
    test('should has Aplabet in password', () => {
        expect(checkAlphabet('m')).toBe(true)
    })
    test('should has Aplabet in password', () => {
        expect(checkAlphabet('A')).toBe(true)
    })
    test('should has not Aplabet in password', () => {
        expect(checkAlphabet('123')).toBe(true)
    })
})
describe('Test Digit in password', () => {
    test('should has digit in password', () => {
        expect(checkSymbol('1234.')).toBe(true)
    })
    test('should has  not digit in password', () => {
        expect(checkSymbol('1234')).toBe(true)
    })
})

describe('Test symbol in password', () => {
    test('should has symbol in password', () => {
        expect(checkSymbol('1234!')).toBe(true)
    })
    test('should has symbol in password', () => {
        expect(checkSymbol('1234.')).toBe(true)
    })
    test('should has not symbol in password', () => {
        expect(checkSymbol('1234')).toBe(true)
    })
})
describe('Test password', () => {
    expect(checkPassword('wen123.')).toBe(false)
})